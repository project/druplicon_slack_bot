<?php
require_once('vendor/autoload.php');

use React\EventLoop\Factory;
use BotMan\BotMan\BotMan;
use BotMan\BotMan\BotManFactory;
use BotMan\BotMan\Drivers\DriverManager;
use BotMan\Drivers\Slack\SlackRTMDriver;

$dotenv = Dotenv\Dotenv::create(__DIR__);
$dotenv->load();

DriverManager::loadDriver(SlackRTMDriver::class);

$slack_api_token = getenv('SLACK_API_TOKEN');

$loop = Factory::create();
/** @var \Botman\Botman\Botman $botman */
$botman = BotManFactory::createForRTM([
  'slack' => [
    'token' => $slack_api_token,
  ],
], $loop);

$botman->hears('keyword', function(Botman $bot) {
  $bot->reply('I heard you! :)');
});

$botman->hears('convo', function(Botman $bot) {
  $bot->startConversation(new ExampleConversation());
});

$loop->run();
